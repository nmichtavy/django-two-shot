from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, CreateCategory, CreateAccount
# Create your views here.


@login_required
def receipts(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": all_receipts
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("/receipts/")
    else:
        form = CreateReceipt()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    all_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_categories": all_categories
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    all_accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": all_accounts
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("/receipts/categories/")
    else:
        form = CreateCategory()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("/receipts/accounts/")
    else:
        form = CreateAccount()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
